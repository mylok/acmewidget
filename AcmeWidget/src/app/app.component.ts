import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Acme Widget Activity Fun Events';
  submitted = false;

  onSubmit() {
    this.submitted = true;
  }
}
