import { Component, OnInit, Input, OnChanges, ViewChild } from '@angular/core';
import { EventService } from '../event.service';
import { Person } from '../person';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-sign-up-list',
  templateUrl: './sign-up-list.component.html',
  styleUrls: ['./sign-up-list.component.css']
})
export class SignUpListComponent implements OnInit, OnChanges {

  displayedColumns: string[] = ['email', 'firstName', 'lastName', 'comment'];

  signuplist: Person[] = [];

  @Input() submitted: boolean = true;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  dataSource = new MatTableDataSource<Person>(this.signuplist);

  constructor(private eventService: EventService) { }

  ngOnInit(): void {
  }

  ngOnChanges() {
    this.getsignupList();
  }

  getsignupList() {
     this.eventService.getSignUpList()
     .subscribe(list=> {
       this.signuplist = list;
       this.dataSource = new MatTableDataSource<Person>(this.signuplist); 
       this.dataSource.paginator = this.paginator;
      });
  }
}
