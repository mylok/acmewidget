export class Person {
    constructor (
        public firstname: string,
        public lastname: string,
        public email: string,
        public activity: string,
        public comment?: string,
    ) {}

}