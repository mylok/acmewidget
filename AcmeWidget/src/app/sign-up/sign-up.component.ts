import { Component, Output, EventEmitter } from '@angular/core';

import { Person } from '../person';
import { EventService } from '../event.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent {
  form: any;
  ngOnInit() {
    throw new Error("Method not implemented.");
  }

  model = new Person("", "", "", "", ""); 

  @Output() submitEmitter = new EventEmitter();

  submitted = false;

  constructor(private eventService: EventService) {}

  onSubmit() { 
    this.eventService.submitSignUp(this.model)
    .subscribe(response=>{
      if (response) {
        this.submitted = true;
        this.submitEmitter.emit();
      }
    });
   }
}

