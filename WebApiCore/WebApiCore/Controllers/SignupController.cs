﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApiCore.Models;

namespace WebApiCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SignupController : ControllerBase
    {
        private readonly SignupContext _context;

        public SignupController(SignupContext context)
        {
            _context = context;
        }

        // GET: api/Signup
        [Microsoft.AspNetCore.Cors.EnableCors]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Signup>>> GetSignup()
        {
            var items = await _context.Signup.ToListAsync();
            return Ok(items);
        }

        //[HttpGet]
        //public IEnumerable<Signup> Get()
        //{
        //    return _context.Signup.ToList();
        //}

        // GET: api/Signup/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Signup>> GetSignup(string id)
        {
            var signup = await _context.Signup.FindAsync(id);

            if (signup == null)
            {
                return NotFound();
            }

            return signup;
        }

        // PUT: api/Signup/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSignup(string id, Signup signup)
        {
            if (id != signup.Email)
            {
                return BadRequest();
            }

            _context.Entry(signup).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SignupExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Signup
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Signup>> PostSignup(Signup signup)
        {
            _context.Signup.Add(signup);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (SignupExists(signup.Email))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetSignup", new { id = signup.Email }, signup);
        }

        // DELETE: api/Signup/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Signup>> DeleteSignup(string id)
        {
            var signup = await _context.Signup.FindAsync(id);
            if (signup == null)
            {
                return NotFound();
            }

            _context.Signup.Remove(signup);
            await _context.SaveChangesAsync();

            return signup;
        }

        private bool SignupExists(string id)
        {
            return _context.Signup.Any(e => e.Email == id);
        }
    }
}
