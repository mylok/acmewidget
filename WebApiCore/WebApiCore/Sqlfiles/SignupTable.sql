/****** Object:  Table [dbo].[Signup]    Script Date: 6/9/2020 4:01:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Signup](
	[Email] [varchar](100) NOT NULL,
	[FirstName] [varchar](50) NOT NULL,
	[LastName] [varchar](50) NOT NULL,
	[Activity] [varchar](100) NOT NULL,
	[Comment] [varchar](500) NULL,
 CONSTRAINT [PK_Signup] PRIMARY KEY CLUSTERED 
(
	[Email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Signup] ([Email], [FirstName], [LastName], [Activity], [Comment]) VALUES (N'agg@yahoo.com', N'Amee', N'Gacia', N'agg', N'agg test')
GO
INSERT [dbo].[Signup] ([Email], [FirstName], [LastName], [Activity], [Comment]) VALUES (N'anthony@msn.com', N'anthony', N'jaswin', N'aj', N'test5')
GO
INSERT [dbo].[Signup] ([Email], [FirstName], [LastName], [Activity], [Comment]) VALUES (N'david@yahoo.com', N'david', N'cen', N'dc', N'test')
GO
INSERT [dbo].[Signup] ([Email], [FirstName], [LastName], [Activity], [Comment]) VALUES (N'DGG@hotmail.com', N'Denny', N'Garner', N'DCF', N'')
GO
INSERT [dbo].[Signup] ([Email], [FirstName], [LastName], [Activity], [Comment]) VALUES (N'Ella@msn.com', N'Ella', N'Lopez', N'Ella', N'Ella')
GO
INSERT [dbo].[Signup] ([Email], [FirstName], [LastName], [Activity], [Comment]) VALUES (N'goi@gmail.com', N'michael', N'goi', N'mg', N'test4')
GO
INSERT [dbo].[Signup] ([Email], [FirstName], [LastName], [Activity], [Comment]) VALUES (N'JR@msn.com', N'Julia', N'Roberts', N'JR', N'JR test')
GO
INSERT [dbo].[Signup] ([Email], [FirstName], [LastName], [Activity], [Comment]) VALUES (N'mali@hotmail.com', N'li', N'ma', N'mali', N'mali')
GO
INSERT [dbo].[Signup] ([Email], [FirstName], [LastName], [Activity], [Comment]) VALUES (N'marko@lee.com', N'marko', N'lee', N'ml', N'test3')
GO
INSERT [dbo].[Signup] ([Email], [FirstName], [LastName], [Activity], [Comment]) VALUES (N'mhenderson@ios.com', N'Martin', N'Henderson', N'mhed', N'')
GO
INSERT [dbo].[Signup] ([Email], [FirstName], [LastName], [Activity], [Comment]) VALUES (N'tony@google.com', N'tony ', N'hawk', N'th', N'test1')
GO
INSERT [dbo].[Signup] ([Email], [FirstName], [LastName], [Activity], [Comment]) VALUES (N'Tony@msn.com', N'Tony', N'Plana', N'Tony', N'Tony')
GO
