using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.TestHost;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using WebApiCore;
using WebApiCore.Controllers;
using WebApiCore.Models;
using Xunit;

namespace web_api_tests
{
    public class UnitTest
    {
        private readonly HttpClient _client;
        public UnitTest()
        {
            var server = new TestServer(new WebHostBuilder()
                .UseEnvironment("Development")
                .UseStartup<Startup>());
            _client = server.CreateClient();
        }
        [Theory]
        [InlineData("GET")]
        public async Task TestAsync(string method)
        {
            var request = new HttpRequestMessage(new HttpMethod(method), $"/api/Signup/");
            var response = await _client.SendAsync(request);
            response.EnsureSuccessStatusCode();
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }
    }
}
